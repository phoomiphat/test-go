package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {
	http.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello world3")
	})
	srv := &http.Server{
		ReadTimeout:    5 * time.Second,
		WriteTimeout:   10 * time.Second,
		Addr:           ":8080",
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(srv.ListenAndServe())
}
